#include <iostream>
#include "LinkedList.h"

using namespace std;

int main()
{
  Node *head;
  Node *first = NULL;
  Node *second = NULL;
  Node *third = NULL;

  // allocate nodes
  first = new Node();
  second = new Node();
  third = new Node();

  // adding values
  first->value = 0;
  second->value = 1;
  third->value = 2;

  // connecting
  first->next = second;
  second->next = third;
  third->next = NULL;

  head = first;

  // print list
  while (head != NULL)
  {
    cout << "Head value = " << head->value << endl;
    head = head->next;
  }
}