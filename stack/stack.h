#pragma once

#include <iostream>

using namespace std;

#define MAX 10

class Stack
{
  int top;

public:
  int arr[MAX];

  Stack()
  {
    top = -1;
  };
  bool push(int x);
  int peek();
  int pop();
  void printStack();
  bool isEmpty(int x);
};

bool Stack::push(int x)
{
  if (top >= (MAX - 1))
  {
    cout << "Stack overflow" << endl;
    return false;
  }
  else
  {
    arr[++top] = x;
    return true;
  }
}

int Stack::pop()
{
  if (top < 0)
  {
    cout << "Stack underflow" << endl;
    return 0;
  }
  else
  {
    int x = arr[top--];
    return x;
  }
}

void Stack::printStack()
{
  for (int i = 0; i < this->top; i++)
  {
    cout << i + 1 << ". " << this->arr[i] << endl;
  }
}
