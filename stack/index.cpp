#include <iostream>
#include "./stack.h"

using namespace std;

// in this case Stack's capacity is 10 items
int main()
{
  Stack stack = Stack();

  stack.push(1);
  stack.push(2);
  stack.push(3);
  stack.push(4);
  stack.push(5);
  stack.push(6);
  stack.push(7);
  stack.push(8);
  stack.push(9);
  stack.push(10);

  // Here will be an overflow
  stack.push(11);

  cout << "\nBefore deleting last added element: \n"
       << endl;
  stack.printStack();

  // this method will remove the last added item
  stack.pop();

  cout << "\n\nAfter deleting last added element: \n"
       << endl;
  stack.printStack();
}