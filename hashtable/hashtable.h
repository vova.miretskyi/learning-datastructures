#include <iostream>
#include <string>
#include <list>

using namespace std;


class HashTable {
  int capacity;
  list<int> *table;

  public: 
  HashTable(int V);

  void insertItem(int key, int data);
  void deleteItem(int key);
  list<int, std::allocator<int>> getItem (int key);

  
  int checkPrime(int n){

    if(n <= 1){
      return 0;
    }

    for(int i = 2; i < n / 2; i++){
      if(n % i == 0){
        return 0;
      }
    }

    return 1;
  }

  int getPrime(int n){
    if(n % 2 == 0){
      n++;
    }

    while(!this->checkPrime(n)){
      n+=2;
    }

    return n;
  }

  int hashFunction(int key){
    return key % this->capacity;
  }

  void displayHash();

  void displayDataByKey(int key);
};

HashTable::HashTable(int c){
  int size = getPrime(c);
  this->capacity = size;
  table = new list<int>[this->capacity];
};

void HashTable::insertItem(int key, int data){
  int index = this->hashFunction(key);
  this->table[index].push_back(data);
};

void HashTable::deleteItem(int key){
  int index = this->hashFunction(key);

  list<int>:: iterator i;

  for(i = this->table[index].begin(); i != this->table[index].end(); i++){
    if(*i != key){
      break;
    }
  }

  if(i != this->table[index].end())
    this->table[index].erase(i);
}


void HashTable::displayHash(){
  for(int i = 0; i < this->capacity; i++){
    cout << "table[" << i << "]";
    for(auto x : this->table[i]){
      cout << "-->" << x;
    }
    cout << endl;
  }
}


list<int, std::allocator<int>> HashTable::getItem(int key){
  int index = this->hashFunction(key);

  return this->table[index];
}


void HashTable::displayDataByKey(int key){
  int index = this->hashFunction(key);

  list<int, std::allocator<int>> data = this->table[index]; 

  list <int, std::allocator<int>>::iterator i;

  for(i = data.begin(); i!= data.end(); i++){
    cout <<"key = " << key << "; hashIndex = " << index << "; value = " << *i << endl;
  }
}