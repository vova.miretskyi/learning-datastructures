#include <iostream>
#include "./hashtable.h"

using namespace std;


int main (){

  int key[] = {231, 321, 212, 321, 433, 262};
  int data[] = {123, 432, 532, 43, 423, 111};

  int size = sizeof(key) / sizeof(key[0]);

  HashTable hashTable(size);

  
  for(int i = 0; i < size; i++){
    hashTable.insertItem(key[i], data[i]);
  }

  hashTable.deleteItem(262);

  hashTable.displayHash();

  hashTable.displayDataByKey(321);

  return 0;
}