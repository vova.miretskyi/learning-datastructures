#include <iostream>
#include "./queue.h"

using namespace std;

// in this case Queue's capacity is 5 items
int main()
{
  Queue queue;

  queue.enQueue(1);
  queue.enQueue(2);
  queue.enQueue(3);

  queue.enQueue(4);
  queue.enQueue(5);
  queue.enQueue(6);

  queue.display();

  queue.deQueue();

  queue.display();
}