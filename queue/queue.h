#pragma once
#include <iostream>

#define SIZE 5

using namespace std;

class Queue
{
private:
  int queue[SIZE];
  int firstElement;
  int lastElement;

public:
  Queue()
  {
    firstElement = 0;
    lastElement = -1;
  }

  bool isEmpty()
  {
    if (this->lastElement < 0)
      return true;

    return false;
  }

  bool isFull()
  {
    if (this->lastElement >= (SIZE - 1))
    {
      return true;
    }

    return false;
  }

  void enQueue(int element)
  {
    if (this->isFull())
    {
      cout << "Queue overflow" << endl;
    }
    else
    {
      this->queue[++this->lastElement] = element;
      cout << "Element " << element << " added to the queue..." << endl;
    }
  }

  int deQueue()
  {
    int element = -1;
    if (this->isEmpty())
    {
      cout << "Queue is empty" << endl;
    }
    else
    {
      element = this->queue[this->firstElement];

      if (this->firstElement >= this->lastElement)
      {
        this->firstElement = 0;
        this->lastElement = -1;
      }
      else
      {
        this->firstElement++;
      }

      cout << "Element " << element << " was removed from the queue..." << endl;
    }

    return element;
  }

  void display()
  {
    if (this->isEmpty())
    {
      cout << "Queue is empty" << endl;
    }
    else
    {
      for (int c = this->firstElement; c < this->lastElement; c++)
      {
        cout << c + 1 << ". " << this->queue[c] << endl;
      }
    }
  }
};